# Big-BWT

Tool to build the BWT and optionally the Suffix Array for highly repetitive files using the approach described in *Prefix-Free Parsing for Building Big BWTs* by Christina Boucher, Travis Gagie, Alan Kuhnle and Giovanni Manzini [1].

Copyrights 2018- by the authors. 
 

## Installation

* Download/Clone the repository
* `make` (create the C/C++ executables) 
* `bigbwt -h` (get usage instruction)

Note that `bigbwt` is a Python script so you need at least Python 3.4 installed.
 

## Sample usage

The only requirement for the input file is that it does not contain the characters 0x00, 0x01, and 0x02 which are used internally by `bigbwt`. To build the BWT for file *yeast.fasta* just type

       bigbwt yeast.fasta

If no errors occurrs the BWT file yeast.fasta.bwt is created: it should be one character longer than the input; the extra character is the BWT eos symbol represented by the ASCII character 0x00. Two important command line options are the window size `-w` and the modulus `-m`. Their use is explained in the paper. 

Using option `-S` it is possible to compute the Suffix Array at the same time of the BWT. 
The Suffix Array is written using 5 bytes per integer (5 is a compilation constant defined in utils.h). This is exactly the same format used by the [pSAscan/SAscan](https://www.cs.helsinki.fi/group/pads/) tools that indeed should produce exactly the same Suffix Array file. 

Using options `-s` and/or `-e` (in alternative to `-S`) it is possible to compute a sampled Suffix Array. Option `-s` generates a `.ssa` file containing the SA entries at the beginning of BWT runs. More precisely, the `.ssa` file contains pairs of 5 bytes integers *<j,SA[j]>* for all indices *j* such that *BWT[j]!=BWT[j-1]*. Option `-e` generates a `.esa` file containing the SA entries at the end of BWT runs: i.e. the pairs *<j,SA[j]>* for all indices *j* such that *BWT[j]!=BWT[j+1]*.

The tool has limited (and experimental) support for multiple threads. 
Use `bigbwt` option `-t` to specify the number of helper threads: in our tests `-t 4` reduced the running time by roughly a factor two.

If you don't trust the output of our tool run bigbwt with option `-c`. 
This will compute the BWT using the SACAK algorithm [1] and compare it with the one computed by bigbwt. Be warned that SACAK, although being the most space economical among linear time algorithms, needs up to *9n* bytes of RAM, since it first compute the Suffix Array and then outputs the BWT (with extension .Bwt).

## Pipeline (from file to file.bwt)

The whole process is divided in three blocks:

1. newscan, pscan (parsing and parallel parsing) (.cpp for files .x for executable, NT.x for no threads)
    - Input: file
    - Output: file.parse, file.dic, file.last, file.occ
2. bwtparse (bwt of a parse)
    - Input: file.parse, file.last, [file.sai]
    - Output: file.ilist, file.bwlast, [file.bwsai]
3. pfbwt (prefix free bwt)
    - Input .occ, .ilist, .bwlast, .dic
    - Output: file.bwt

## Notation

  - T ... input text (T = (0x2)file_content(0x2)^wsize)
  - P ... parse
  - D ... dictionary
  - p ... number of phrases in parse
  - d ... number of dictionary words

## Description of the intermediate files

- .dict
  - containing the dictionary words in lexicographic order with a 0x1 at the end of each word and a 0x0 at the end of the file.
  - the smallest dictionary word is d0 =$.... that occurs once (but not is not encoded in the dictionary file)
  - first (imaginary) word is a dummy word therefore the parse identifies words by 1-based lexicographic rank
  - Size: |D| + d + 1 (|D| is the sum of the word lengths)
- .occ
  - the number of occurrences of each word in lexicographic order. We assume the number of occurrences of each word is at most 2^32-1
  - size: 4d bytes (32 bit ints)
- .parse
  - containing the parse P with each word identified with its 1-based lexicographic rank (ie its position in D). We assume the number of distinct words is at most 2^32-1,
  - Note that the words in the parsing overlap by wsize
  - 1-based
  - size: 4p bytes (32 bit ints)
- .last
  - file.last
    - containing the character in position w+1 from the end for each dictionary word {strange}
    - last[i] = dict[P[i]][-w]
    - size: p
- .bwlast
  - permutation of file.last according to the BWT permutation such that: {wrong -> sizes differ}
  - bwlast[i] = dict[P[SA[i]-2]][-w] (special cases are described below)
  - file.bwlast[i] is the char from P[SA[i]-2] (if SA[i]==0 , BWT[i]=0 and file.bwlast[i]=0, if SA[i]==1, BWT[i]=P[0] and file.bwlast[i] is taken from P[n-1], the last word in the parsing).  
  - The remapping is done as follows, let P[0] P[1] ... P[n-1] P[n] denote the input parse where P[n]=0 is an extra EOS symbol added here   
  -  For j=0,...,n, if BWT[j] = P[i] then:
  -    bwisa[j]  is the ending position+1 of P[i] in the original text
  -    bwlast[j] is the char immediately before P[i] in the original text, ie the char in position w from the end of P[i-1]
  -  Since P[n]=0, and P[0] = $abc... is the only phrase starting with $,
  -  it is P[n]<P[0]<P[i] and BWT[0]=P[n-1], BWT[1]=P[n]=0. We have
  -    bwisa[0]  = ending position + 1 of P[n-1],
  -    bwlast[0] = char in position w from the end of P[n-2]
  -    bwisa[1] and bwlast[1] are dummy values since P[n] is not a real phrase
  -  For some j it is BWT[j]=P[0], for that j we set
  -    bwisa[j] = ending position + 1 of P[0] as expected
  -    bwlast[j] = char in position w from the end in P[n-1] (it should formally be P[n] but again that is a dummy symbol and we skip it)
  - size: p+1
- .ilist (inverted list occurrence)
  - For each dictionary word in lexicographic order, the list of BWT positions where that word appears (ie i \in ilist(w) <=> BWT[i]=w). There is also an entry for the EOF word which is not in the dictionary but is assumed to be the smallest word.  
  - istart[] and islist[] are used together. For each dictionary word i (considered in lexicographic order) for k = istart[i]...istart[i+1]-1 ilist[k] contains the ordered positions in BWT(P) containing word i
  - size: 4p+4
- ".istart" (inverted lists occurrence start)
  - delimiters for .ilist
  - created in pfbwt using .occ as just a prefix sum of entries (line 387: convert occ entries into starting positions inside ilist)
  - size: 4d
  
  
## Example

TODO: Change the input to something that is less rubbish

Elements in parenthesis () are not part of the file.

- file: "afafadf line[: line[: line[: afafad"
- window: 4
- modulo: 10
- dict: [('$') '0x2afafadf lin', ' line[:', 'ne[: afafad0x2^w', 'ne[: lin']
- triggers: " lin", "ne[:", 0x2
- parse: 1 2 4 2 4 2 3 ($)
- bwt of parse: 3 $ 4 4 1 2 2 2
- list: f i : i : i d
- bwlist: i 0 i i d : : f
- ilist (1) [4] [5 6 7] [0] [2 3]
- occ: 1 3 1 2
- bwt: "d:::f[[[eeeffff  aannndaaaalll   iii"

## Further information

Detailed description of the pipeline can be found in `newscan.cpp` .

## References

\[1\]  Christina Boucher, Travis Gagie, Alan Kuhnle and Giovanni Manzini 
*Prefix-Free Parsing for Building Big BWTs* [Proc. WABI '18](http://drops.dagstuhl.de/opus/volltexte/2018/9304/).

\[2\] Nong, G., 
*Practical linear-time O(1)-workspace suffix sorting for constant alphabets*, ACM Trans. Inform. Syst., vol. 31, no. 3, pp. 1-15, 2013

